#! /usr/bin/perl

# Copyright (C) 2014 Ivo De Decker

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


use strict;
use Data::Dumper;
$Data::Dumper::Sortkeys=1;
use DBI;
use POSIX;

my $real = 0;
my $mailcommand = "cat" ;
if( $ARGV[0] eq '--for-real' ) {
    $mailcommand = '/usr/lib/sendmail -t -f noreply@release.debian.org' ;
    $real = 1;
    shift ;
}

my $dbconnect_udd ="dbi:Pg:service=udd";
my $dbh_udd = DBI->connect($dbconnect_udd);
my $autoremoval_table = "testing_autoremovals";

my $dbconnect_release ="dbi:Pg:service=release";
my $dbh_release = DBI->connect($dbconnect_release);
my $mailtable = "autoremoval_mails";
# create table autoremoval_mails(source text, version text, notification bigint);
# create index autoremoval_mails_source_idx on autoremoval_mails(source);

my $debug = 1;
my $now = time;
my $mailstamp = ();

my $mailinterval = 20*3600*24;

sub do_query {
    my $handle = shift;
    my $query = shift;

    my $sthc = $handle->prepare($query);
    $sthc->execute() or die("failed to run query: $query\n");
    return $sthc;
}

sub get_buginfo {
    my $bugid = shift;
    my $query = "select package,title from bugs where id=?";
    my $sthc = $dbh_udd->prepare($query);
    $sthc->execute($bugid) or die("failed to run query: '$query' ($bugid)\n");
    if (my $pg = $sthc->fetchrow_hashref()) {
        my $title = $pg->{'title'};
        $title =~ s/^src://;
        my $package = $pg->{'package'};
        $package =~ s/^src://;
        if ($title !~ m/^$package:/) {
            $title = "$package: $title";
        }
        my $info = $title;
        return $info;
    }
}

sub show_bugs {
    my $bugs = shift;
    my $header = shift;

    my $info = "";

    my @bugs = split(",",$bugs);
    if (@bugs > 0) {
        $info .= "$header\n";
        foreach my $bug (@bugs) {
            $info .= $bug.": ".get_buginfo($bug)."\n";
        }
        $info .= "\n";
    }
    return $info;
}

my $mail_query = "select * from $mailtable";
my $sthc = do_query($dbh_release,$mail_query);
while (my $pg = $sthc->fetchrow_hashref()) {
    $mailstamp->{$pg->{'source'}}->{$pg->{'version'}} = $pg->{'notification'}
        unless ($pg->{'notification'} < $mailstamp->{$pg->{'source'}}->{$pg->{'version'}});
}

my $ar_query = "select * from $autoremoval_table order by removal_time";

my $sthc_stamp = $dbh_release->prepare("insert into $mailtable (source,version,notification) values (?,?,?)");

my $sthc = do_query($dbh_udd,$ar_query);
while (my $pg = $sthc->fetchrow_hashref()) {
    next if ($mailstamp->{$pg->{'source'}}->{$pg->{'version'}} > $now - $mailinterval);
    my $removaldate = strftime("%Y-%m-%d",localtime($pg->{'removal_time'}));

    my $subject = $pg->{'source'}." is marked for autoremoval from testing";
    my $info = $pg->{'source'}." ".$pg->{'version'}." is marked for autoremoval from testing on $removaldate\n\n";

    $info .= show_bugs($pg->{"bugs"},"It is affected by these RC bugs:");
    $info .= show_bugs($pg->{"bugs_deps"},"It (build-)depends on packages with these RC bugs:");

    open MAIL, "|-", $mailcommand or die "Cannot spawn $mailcommand for $pg->{'source'}" ;

    print MAIL "From: Debian testing autoremoval watch <noreply\@release.debian.org>\n".
        "Subject: $subject\n".
        "To: $pg->{'source'}\@packages.debian.org\n".
        "Bcc: $pg->{'source'}_summary\@packages.qa.debian.org, _auto\@packages.qa.debian.org\n".
        "\n$info";

    if ($real) {
        $sthc_stamp->execute($pg->{'source'},$pg->{'version'},$now) or
            die("failed to update stamp info for ($pg->{'source'},$pg->{'version'},$now)\n");
    }
}

# TODO keep them as an historic record of the mails we sent?
my $query_stamp = "delete from $mailtable where notification < ".($now - $mailinterval);
do_query($dbh_release,$query_stamp);


