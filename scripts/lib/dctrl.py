#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Functions that obtain information from Sources/Packages with grep-dctrl."""

from __future__ import print_function

import re
import sys
import subprocess

import apt_pkg
apt_pkg.init()

# TODO Abstract stuff into a GrepArchive class?

##

def get_disappeared_binaries(src_pkgs, suite_from='unstable', suite_to='testing'):
    """Return a dict pkg -> srcpkg where the keys are disappeared binary packages.

    A disappeared binary package is one that is present in $suite_to, but not in
    $suite_from.
    """
    def get_binary_pkgs(src_pkgs, suite):
        cmd = [ 'grep-archive', '%s:ALL:source' % (suite,), '-P',
                '-e', '^(%s)$' % '|'.join(src_pkgs), '-s', 'Package,Binary' ]

        d = {}
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        parser = apt_pkg.TagFile(p.stdout)
        step = parser.step
        section = parser.section

        while step():
            d[section['Package']] = set(
                    re.sub(',', '', section['Binary']).split())

        diff = set(src_pkgs) - set(d)
        if diff:
            print('I: could not find these source packages in %s: %s' %
                  (suite, ', '.join(diff)), file=sys.stderr)
            for pkg in diff:
                d[pkg] = set()

        return d

    d = {}
    to_pkgs = get_binary_pkgs(src_pkgs, suite_to)
    from_pkgs = get_binary_pkgs(src_pkgs, suite_from)

    for pkg in to_pkgs:
        disappeared = to_pkgs[pkg] - from_pkgs[pkg]
        if not disappeared:
            print('W: no disappeared packages found for %s' % pkg, file=sys.stderr)
        else:
            d.update((binary, pkg) for binary in disappeared)

    return d
