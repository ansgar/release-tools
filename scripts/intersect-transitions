#! /usr/bin/python3
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Check whether two or more transitions intersect.

A group of transitions intersects if at least one package in testing depends on
disappeared binaries from at least two of the transitions.

Use this program by giving a list of source package names that represent
transitions. The script will figure out the disappeared packages by itself.

If a transition is not uploaded to unstable yet, you can:

  (a) pass it as "srcname/experimental" if it's uploaded to experimental.
  (b) pass it as "srcname:oldpk1,oldpkg2[,...] if it's not uploaded at all yet.
"""
from __future__ import print_function

import re
import optparse
import subprocess

import apt_pkg
apt_pkg.init()

from lib import dctrl

##

def main():
    options, args = parse_options()

    disappeared = {}
    unstable = set()
    experimental = set()

    for pkg in args:
        if not re.search(r'[:/]', pkg):
            unstable.add(pkg)
        elif pkg.endswith('/experimental'):
            experimental.add(re.sub(r'/experimental$', '', pkg))
        else:
            src, pkgs = pkg.split(':', 1)
            pkgs = pkgs.split(',')
            disappeared.update((p, src) for p in pkgs)

    for pkgs, suite in (unstable, 'unstable'), (experimental, 'experimental'):
        if pkgs:
            disappeared.update(dctrl.get_disappeared_binaries(pkgs, suite, 'testing'))

    ##

    cmd = [ 'grep-archive', 'testing:ALL:%s' % (options.arch,), '-FDepends',
            '-e', '|'.join('(^| )%s($|[ ,])' % (r,) for r in disappeared.keys()),
            '-s', 'Package,Source,Depends' ]

    d = {}
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    parser = apt_pkg.TagFile(p.stdout)
    step = parser.step
    section = parser.section

    while step():
        pkg = section['Package']
        src = section.get('Source') or pkg

        if ' ' in src:
            src = src.split()[0]

        for dep in apt_pkg.parse_depends(section['Depends']):
            for d_pkg, d_ver, d_rel in dep:
                try:
                    x = disappeared[d_pkg]
                except KeyError:
                    pass
                else:
                    d.setdefault(src, set()).add(x)

    ##

    for pkg in d:
        if len(d[pkg]) > 1:
            print('%s depends on: %s' % (pkg, ', '.join(d[pkg])))

##

def parse_options():
    p = optparse.OptionParser(usage='%s srcpkg1 srcpkg2 [...]')

    p.add_option('-a', dest='arch')

    p.set_defaults(arch='i386')

    options, args = p.parse_args()

    if len(args) <= 1:
        p.error('need two or more transitions')

    return options, args

##

if __name__ == '__main__':
    main()
