#! /bin/sh

set -u

# These are the id numbers in projectb.
TESTING=4
UNSTABLE=5
TPU=6

if [ "${1-}" = "-v" ]; then
    VERBOSE="TRUE"
    COLUMN_NAME="missing_arches"
    SORT_BIT=""
else
    VERBOSE="FALSE"
    COLUMN_NAME="built"
    SORT_BIT="built desc,"
fi

##

cat <<EOF
Packages in t-p-u
=================

EOF

psql service=projectb <<EOF
select source,
       tpu_version as version,

       -- Now come the various version checks: testing << tpu <= unstable
       -- both for the source versions involved, as well for the binary
       -- versions of all binary packages.
       case when (
           tpu_version > testing_version AND
           tpu_version <= unstable_version AND
           (select count(package) from

             (select package, architecture, version as testing_version
              from bin_associations ba (ba_id, suite, id) natural join binaries
              where suite = $TESTING and source = testing_id) j1

             natural join

             (select package, architecture, version as tpu_version
              from bin_associations ba (ba_id, suite, id) natural join binaries
              where suite = $TPU and source = tpu_id) j2

             natural join

             (select package, architecture, version as unstable_version
              from bin_associations ba (ba_id, suite, id) natural join binaries
              where suite = $UNSTABLE and source = unstable_id) j3

           where tpu_version < testing_version OR
                 tpu_version > unstable_version) = 0)
         then 'yes'
         else 'no'
       end as version_ok,

       -- Now comes a column indicating whether the package is ready
       -- or not. With -v, a list of missing arches is printed; without
       -- it, just a yes/no boolean. (This boolean just checks if the
       -- number of binaries in t-p-u is greater or equal than the
       -- number in testing.)
       case when ($VERBOSE)
       then
       (select space_separated_list(distinct arch_string) from
           (select package, architecture
                from bin_associations ba (ba_id, suite, id)
                natural join binaries
                where suite = $TESTING and source = testing_id
            except
            select package, architecture
                from bin_associations ba (ba_id, suite, id)
                natural join binaries
                where suite = $TPU and source = tpu_id) s,
           architecture a
           where a.id = s.architecture)
       else
       case when (
           (select count(id)
             from bin_associations ba (ba_id, suite, id)
             natural join binaries
             where suite = $TPU and source = tpu_id) >=
           (select count(id)
             from bin_associations ba (ba_id, suite, id)
             natural join binaries
             where suite = $TESTING and source = testing_id))
         then 'yes'
         else 'no'
       end
       end as $COLUMN_NAME
from

  -- We produce a table with source + {testing,tpu,unstable}_{version,id}
  -- for stuff that is in t-p-u.

  (select s.source, s.version as testing_version, s.id as testing_id
    from source s join src_associations sa (id, suite, srcid)
    on sa.suite = $TESTING and sa.srcid = s.id) j1

  natural join

  (select s.source, s.version as tpu_version, s.id as tpu_id
    from source s join src_associations sa (id, suite, srcid)
    on sa.suite = $TPU and sa.srcid = s.id) j2

  natural join

  (select s.source, s.version as unstable_version, s.id as unstable_id
    from source s join src_associations sa (id, suite, srcid)
    on sa.suite = $UNSTABLE and sa.srcid = s.id) j3

  -- And keep the ones where testing_version < tpu_version.

  where testing_version < tpu_version

  order by version_ok desc, $SORT_BIT source asc;
EOF

# vi: et
