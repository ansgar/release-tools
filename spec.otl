Handling of hints and hint files
	The "hint" script
		: A single script is provided to create hints, and perform checks and
		: updates on the hint files. It replaces the old "cleanhints",
		: "e2remove", and "e2easy", but it'd be trivial to provide aliases for
		: those.
		
		Creating hints from the command line
			: Use the script like this:
			:
			: 	hint <hintname> pkg1 [ pkg2 ... ]
			:
			: It will output a line suitable for inclusion in a hint file.
			: Version numbers will be grabbed from testing or unstable
			: depending on the type of the hint. Binary package names can be
			: given, but a warning will be emitted.
			
		Creating hints from your editor (using "hint" as a filter)
			: The script can be run with no arguments, and then it will act as
			: a filter, reading hints from standard input, and printing them
			: in standard output with added or updated version numbers.
			:
			: To make use of this in eg. vim, write a line like "remove foo",
			: go to normal mode and, while over the line, type "!!hint<Enter>".
			
		Checking your hint file for done hints, or hints with out of date versions
			: Use the script like this:
			:
			: 	hint check [ who ]
			:
			: It will print in standard output a report of the hints that are
			: already done or out of date. "who" defaults to $USER.
			
		Automatically cleaning of your hint file
			: Use:
			:
			: 	hint clean [ who ]
			:
			: To move done hints after the "finished" line. All comments in
			: the same paragraph as the hint will be moved with it. If one of
			: the comments is in the format "yyyy-mm-dd" or "yyyymmdd", a
			: "; done yyyymmdd" will be appended to it.
			
		Warnings about overriden hints
			: To check whether two or more hint files contain hints for the
			: same package, this can be run:
			:
			: 	hint conflicts
			:
			: By default, it will warn to standard output. If the "--mail"
			: option is given, mail will be sent to the involved parties
			: instead.
			
		Grepping through all hint files
			: You can grep through the contents of all hint files, but only up
			: to their "finished" line, with:
			:
			: 	hint grep pkgname


# vi: ts=4
